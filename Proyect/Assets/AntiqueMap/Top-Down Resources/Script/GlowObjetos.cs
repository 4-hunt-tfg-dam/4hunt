﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class GlowObjetos : MonoBehaviour
{


    public GameObject highligth;

    private void Reset()
    {
        GetComponent<CircleCollider2D>().isTrigger = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "HiderController")
        {
            Debug.Log("No te choques");
            highligth.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "HiderController")
        {
            highligth.SetActive(false);
        }
    }
    /* private SpriteRenderer obj;
     public float velocidadIluminacion;

     private Color progresoColor;
     private Color colorObjetivo;

     public void Awake()
     {
         obj = gameObject.GetComponent<SpriteRenderer>();
     }
     private void OnTriggerEnter2D(Collider2D other)
     {
         colorObjetivo = new Color(1, 1, 1, 1);
     }

     private void OnTriggerExit2D(Collider2D other)
     {
         colorObjetivo = new Color(1, 1, 1, 0);
     }

     private void Update()
     {
         progresoColor = Color.Lerp(progresoColor, colorObjetivo, velocidadIluminacion * Time.deltaTime);

         obj.color = progresoColor;

 
    }*/
}
