﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cainos.PixelArtTopDown_Basic
{
    //cuando un objeto sale del trigger, se pone el layer asignado y el numero que ocupa dentro del mismo
    //se usa en escaleras y puente para que el jugador viaje entre layers
    public class LayerTrigger : MonoBehaviour
    {
        public string layer;
        public string sortingLayer;

        private void OnTriggerExit2D(Collider2D collider)
        {
            collider.gameObject.layer = LayerMask.NameToLayer(layer); //Layer que declaras en el jugador

            collider.gameObject.GetComponent<SpriteRenderer>().sortingLayerName = sortingLayer; //Sorting Layer que declaras en el jugador
            SpriteRenderer[] srs = collider.gameObject.GetComponentsInChildren<SpriteRenderer>(); //Objeto Sprite que creas con los datos
            foreach ( SpriteRenderer sr in srs)//Colocar en cada sprite renderer los datos al salir del trigger
            {
                sr.sortingLayerName = sortingLayer;
            }
        }

    }
}
