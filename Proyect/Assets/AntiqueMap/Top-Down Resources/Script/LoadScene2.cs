﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DentedPixel;

public class LoadScene2 : MonoBehaviour
{
    public int sceneID;
    public GameObject loadingScreenObj;
    public Slider slider;
    public int time;


    public void Start()
    {
        StartCoroutine(LoadingScreen());
    }

    IEnumerator LoadingScreen()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneID);
        loadingScreenObj.SetActive(true);
        operation = SceneManager.LoadSceneAsync(0);
        operation.allowSceneActivation = false;

        while (operation.isDone == false)
        {
            slider.value = operation.progress;
            if (operation.progress == 0.9f)
            {
                LeanTween.scaleX(loadingScreenObj, 1, time);
                slider.value = 1f;
                operation.allowSceneActivation = true;
            }
            yield return null;

        }
    }
}
