﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementHider : MonoBehaviour
{
    private float moveSpeed = 20f;
    private Vector3 moveDir;
    private Rigidbody2D body2D;
   // Transform myAvatar;
    private Animator myAnim;
 

    private void Awake()
    {
       body2D = GetComponent<Rigidbody2D>();
       myAnim = GetComponent<Animator>();
       // myAvatar = transform.GetChild(0);
    }



    // Update is called once per frame
   private void Update()
    {
        //Optimizacion para implementarle la direccion al rigidbody
        float moveX = 0f;
        float moveY = 0f;
        if (Input.GetKey(KeyCode.W))
        {
            moveY = 1f;
           // transform.position += new Vector3(0, +1);
        }

        if (Input.GetKey(KeyCode.S))
        {
            moveY = -1f;
            // transform.position += new Vector3(0, -1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            moveX = -1f;
           // transform.position += new Vector3(-1, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            moveX = 1f;
           // transform.position += new Vector3(+1, 0);
        }

        moveDir = new Vector3(moveX, moveY).normalized;//normalized hace que la velocidad no aumente al ir en diagonal. W+A por ejemplo doblaria la speed


     
       /* if(moveDir != Vector3.zero)
        {
            myAnim.SetBool("isMoving", true);
        }
        else
        {
            myAnim.SetBool("isMoving", false);
        }*/
    }
    private void FixedUpdate()
    {
       body2D.velocity = moveDir * moveSpeed;
     
    }
}

