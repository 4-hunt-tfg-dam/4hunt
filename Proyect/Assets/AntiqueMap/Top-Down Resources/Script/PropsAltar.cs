﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


//Cuando algo se posiciona sobre el altar se ilumina mediante un trigger
namespace Cainos.PixelArtTopDown_Basic
{
 
    public class PropsAltar : MonoBehaviour
    {

        public List<SpriteRenderer> runas;
        private GameObject EscapeText;
        public float velocidadIluminacion;
        public bool interactable = false;
        private Color progresoColor;
        private Color colorObjetivo;


   
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Hider")
            {    
                    other.GetComponent<Transformacion>().rc = gameObject;
                    colorObjetivo = new Color(1, 1, 1, 1);
                    interactable = true;
                
            }
            if (gameObject.transform.tag == "Escape" && other.tag == "Hider" && RoomManager.Instance.EscapeAvailable == true)
            {
                //Animacion de escapar
                Animator escapeAnimation = other.GetComponent<Animator>();
                if(escapeAnimation)
                escapeAnimation.SetTrigger("Escape");

                StartCoroutine(Escape(other.gameObject, escapeAnimation.GetCurrentAnimatorStateInfo(0).length));

                RoomManager.Instance.HiderScaped++;

            }
        }
        
        IEnumerator Escape(GameObject other, float delay = 0)
        {
            var children = other.GetComponentsInChildren<Transform>();
            foreach (var child in children)
                if (child.name == "EscapeText")
                    EscapeText = child.gameObject;

            EscapeText.SetActive(true);
            yield return new WaitForSeconds(delay);
            Debug.Log("dESTROY" + other);
            Destroy(other);
           
        }
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == "Hider")
            {
                colorObjetivo = new Color(1, 1, 1, 0);
                interactable = false;
            }    
        }

        private void Update()
        {
            progresoColor = Color.Lerp(progresoColor, colorObjetivo, velocidadIluminacion * Time.deltaTime);
                foreach (var r in runas)
            {
                r.color = progresoColor;
            }
        }
    }
}
