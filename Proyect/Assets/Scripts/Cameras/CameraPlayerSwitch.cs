﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayerSwitch : MonoBehaviour
{

    private Transform currentTarget;
    //public GameObject teamCameras;
    //private Transform player;
    private GameObject [] targets;
    private float speed;
    // Start is called before the first frame update
    void Awake()
    {
        
       // player = GameObject.FindGameObjectWithTag("HunterDeath").transform;
        targets = GameObject.FindGameObjectsWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            foreach (GameObject targets in targets)
            {
               currentTarget = targets.transform;
                MoveCamera();
            }
        }
    }
  
    public void MoveCamera()
    {
        Vector3 newPos = new Vector3(currentTarget.position.x, currentTarget.position.y, -10);
        transform.position = Vector3.MoveTowards(transform.position, newPos, speed * Time.deltaTime);
    }
}
