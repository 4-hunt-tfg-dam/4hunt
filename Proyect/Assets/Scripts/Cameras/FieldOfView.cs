﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{ 

    private Mesh mesh;
    private Vector3 origen;

    public float fov = 360f;
    public int nAristas = 360;// cuanto mas aristas mas calidad de renderizado del campo de vision
    public float anguloInicial = 0; //donde se empieza a crear la malla
    public float distanciaVision = 0f;//radio de la malla
    public GameObject Player;
    private LayerMask layerMask;


    // Start is called before the first frame update

    
    void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh; //asignamos a mesh filter del gameobject
    }

    // Update is called once per frame
    void Update()
    {
        layerMask = Player.layer;//Cogemos enq ue layer esta neustro jugador
        GenerarMalla();
    }

    private void GenerarMalla()
    {
        Vector3[] vertices = new Vector3[nAristas + 2]; // lista de vertices de la red malla de vision dividida en triangulos(+1 por el centro del circulo)
        int[] triangulos = new int[nAristas * 3]; // contiene trios con los indices de los vertices que conforman cada triángulo(*3 debido a que por cada triangulo 3 aristas)

        float anguloActual = anguloInicial;// almacenamos la posicion actual del angulo que creamos
        float incrementoAngulo = fov / nAristas;//grados totales entre numero de aristas

        vertices[0] = origen;

        int indiceVertices = 1;//porque ya esta inicializado por vector origen
        int indiceTriangulos = 0;


        for (int i = 0; i <= nAristas; i++){//se ejecuta una vez por arista para calcular los vertuices

            //0. Para colision con paredes y objetos utilizamos Raycast

            RaycastHit2D raycastHit2D = Physics2D.Raycast(origen, GetVectorFromAngle(anguloActual), distanciaVision, layerMask);


            //1. Calcular posicion de nuevo vertice

            Vector3 verticeActual;

            if(raycastHit2D.collider == null)
            {
               verticeActual = origen + GetVectorFromAngle(anguloActual) * distanciaVision; // crea triangulos en abanico sin cambios
            }
            else
            {
                verticeActual = raycastHit2D.point;//Si encuentra obstucalo nuestro vertice es el punto de colision
            }
             


            //2. Guardar vertice en array de vertices

            vertices[indiceVertices] = verticeActual;//guardas el vertice creado en el array de vertices


            //3. Guardar indices en array de triangulos
            //Si es la primera vuelta del bucle tenermos 1 arista y 2 vertices

            if (i != 0) {
                    triangulos[indiceTriangulos] = 0;//vertice de origen
                    triangulos[indiceTriangulos + 1] = indiceVertices - 1;//vertice anterior
                    triangulos[indiceTriangulos + 2] = indiceVertices;//vertice actual

                indiceTriangulos += 3;//creado el triangulo actualizamos
                }


            //4. Actualizar indices de los arrays

            indiceVertices++;
            anguloActual -= incrementoAngulo;//al crearse los triangulo en el sentido de las agujas del reloj en unity se decrementa
        }  
            
            
            
            
            
            //vertices = new Vector3[]
        //{
        //    new Vector3 (0,0,0),
        //    new Vector3 (1,0,0),// componente z siempre es 0 solo varia la posicion de x e y para el triangulo debido a que es bidimensional
        //    new Vector3 (0,-1,0)
        //};

        triangulos = new int[]
        {
            0, 1, 2 // multiplo de 3 porque los triangulos tienen 3 vertices
        };

        mesh.vertices = vertices;
        mesh.triangles = triangulos;
    }
    Vector3 GetVectorFromAngle(float angle)//Obtiene un vector a partir de un angulo(formula matematica)
    {
        float anguloRad = angle * (Mathf.PI / 180f);
        return new Vector3(Mathf.Cos(anguloRad), Mathf.Sin(anguloRad)); // Cos y Sin para el coseno y el seno del angulo
    }
}
