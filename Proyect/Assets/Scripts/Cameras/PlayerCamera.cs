﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    private PhotonView PV;
    public GameObject Camera;
    public GameObject Canvas;
    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();

        if (PV.IsMine)
        {
            Canvas.SetActive(true);
            Camera.SetActive(true);
            foreach (GameObject gameObj in GameObject.FindObjectsOfType<GameObject>())
            {
                if (gameObj.name == "Highligth")
                {
                    gameObj.SetActive(false);
                }
            }
        }
    }
}

