﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;



public class ChatManager : MonoBehaviour
{
   
    public PhotonView PV;
    public GameObject ChatPopUp;
    public TMP_Text UpdatedText;
    public TMP_InputField ChatInputField;
    public bool isChatFocused;
    public static ChatManager Instance;



    private bool DisableSend;
    // Start is called before the first frame update
    // Update is called once per frame

    private void Awake()
    {
        Instance = this;
    }
    void Update()
    {
        if (PV.IsMine)
        {
            if (Input.GetKeyDown(KeyCode.Space) && !isChatFocused)
            {
                isChatFocused = true;

                ChatInputField.ActivateInputField();
            }
            else if (Input.GetKeyDown(KeyCode.Space) && isChatFocused)
            {
                isChatFocused = false;

                ChatInputField.DeactivateInputField();
            }

            if (!DisableSend && ChatInputField.isFocused)//Es decir que estemos editando el Input
            {
                if (ChatInputField.text != "" && ChatInputField.text.Length > 0 && Input.GetKeyDown(KeyCode.Tab))
                {
                    if (PlayerManager.Instance.myTeam == 1)
                    {
                        Debug.Log("ChatHunter");
                        PV.RPC("SendMessage", PhotonNetwork.PlayerList[0], ChatInputField.text);
                        PV.RPC("SendMessage", PhotonNetwork.PlayerList[2], ChatInputField.text);
                        ChatPopUp.SetActive(true);
                        ChatInputField.text = "";
                        DisableSend = false;
                    }
                    else if (PlayerManager.Instance.myTeam == 2)
                    {
                        Debug.Log("ChatHider");
                        PV.RPC("SendMessage", PhotonNetwork.PlayerList[1], ChatInputField.text);
                        PV.RPC("SendMessage", PhotonNetwork.PlayerList[3], ChatInputField.text);
                        PV.RPC("SendMessage", PhotonNetwork.PlayerList[4], ChatInputField.text);
                        ChatPopUp.SetActive(true);
                        ChatInputField.text = "";
                        DisableSend = false;
                    }
                }
            }
        }
      
    }


    [PunRPC]
    private new void SendMessage(string message)
    {
        UpdatedText.text = message;

        StartCoroutine("Remove");
    }

    IEnumerator Remove()
    {
        yield return new WaitForSeconds(2f);
        ChatPopUp.SetActive(false);
    }

    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(ChatPopUp.activeSelf);
        }else if (stream.IsReading)
        {
            ChatPopUp.SetActive((bool)stream.ReceiveNext());
        }
    }
}
