﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using TMPro;
using UnityEngine.UI;




public class PlayerLobbyController : MonoBehaviourPunCallbacks
{

    [SerializeField] TMP_Text text;
    [SerializeField] TMP_Text ready_text;
    public static PlayerLobbyController Instance;

    private ExitGames.Client.Photon.Hashtable _myCustomProperties = new ExitGames.Client.Photon.Hashtable();
    public string result;
    public bool isPlayerReady = false;
    Player player;
       public void SetUp(Player _player)
    {
       // isPlayerReady = false;
        player = _player;
        SetPlayerText(_player);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if(targetPlayer != null && targetPlayer == player)
        {
            if(changedProps.ContainsKey("PlayerReady"))
            SetPlayerText(targetPlayer);
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
       if(player == otherPlayer)
        {
            Destroy(gameObject);
        }
    }

    public override void OnLeftRoom()
    {
        Destroy(gameObject);
    }

    public void SetPlayerText(Player _player)
    {
        result = "Waiting player...";
        text.text = _player.NickName;
        if (_player.CustomProperties.ContainsKey("PlayerReady"))
            result = (string)_player.CustomProperties["PlayerReady"];
        ready_text.text = result;
        ready_text.color = new Color(1.0f, 0.64f, 0.0f);
    }

    public  void OnReadyForGame()
    {
        if (isPlayerReady)
        {
 
            result = "Waiting player...";      
            isPlayerReady = false;
        }
        else
        {
            result = "Ready";
            isPlayerReady = true;
        }
  
        _myCustomProperties["PlayerReady"] = result;
        PhotonNetwork.SetPlayerCustomProperties(_myCustomProperties);
    }


    public bool getPlayerReady(Player _player)
    {

        if ((string)_player.CustomProperties["PlayerReady"] == "Ready")
        {
            return true;
        }
        else
            return false;
            
    }
        public bool getIfReady()
    {
        return isPlayerReady;
    }

  

}

