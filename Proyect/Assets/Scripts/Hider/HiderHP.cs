﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HiderHP : MonoBehaviour
{
    private PhotonView PV;
    public int health = 100;
    public int countAux;
    public HealthBar healthBar;
    public GameObject deadText;
   // public GameObject textureBlood;
    private Camera deathCamera;


    private void Awake()
    {
        //deathCamera = Camera.FindObjectOfType<Camera>();
        PV = GetComponent<PhotonView>();
    }
    //public GameObject effectDeath; //todavia no tenemos este efecto creado.
    public void HealthDown(int damage) //public para poder llamar al metodo desde PlayerAimGun
    {
        
        health = health - damage;
        Debug.Log("vida"+ health);
        healthBar.SetHealth(health);
       // Instantiate(textureBlood, transform.position, Quaternion.identity);

        if (health <= 0)
        {
            PV.RPC("PlayerDead", RpcTarget.All);
        }
    }

    [PunRPC]
    void PlayerDead()
    {
        countAux = RoomManager.Instance.HiderDeaths++;
       
        Animator deathAnimation = gameObject.GetComponent<Animator>();
        if (deathAnimation)
            deathAnimation.SetTrigger("Dead");

        StartCoroutine(Die(5f));
    }
    IEnumerator Die(float delay)
    {
        deadText.SetActive(true);
        yield return new WaitForSeconds(delay);

        Destroy(gameObject);
    }
}
