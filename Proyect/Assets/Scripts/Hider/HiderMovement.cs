﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HiderMovement : MonoBehaviour
{
    private PhotonView PV;
    private float moveSpeed = 20f;
    private Vector3 moveDir;
    private Rigidbody2D body2D;
    public GameObject Hider;
   // Transform myAvatar;
    private Animator myAnim;
 

    private void Awake()
    {
       body2D = GetComponent<Rigidbody2D>();
       myAnim = GetComponent<Animator>();
        PV = GetComponent<PhotonView>();
        // myAvatar = transform.GetChild(0);
    }

    // Update is called once per frame
    private void Update()
    {
        foreach (GameObject gameObj in GameObject.FindObjectsOfType<GameObject>())
        {
            if (gameObj.name == "Highligth")
            {
                gameObj.SetActive(true);
            }
        }

        if (!PV.IsMine)
            return;

        Vector3 spriteFlip = Hider.transform.localScale;
        //Optimizacion para implementarle la direccion al rigidbody
        float moveX = 0f;
        float moveY = 0f;

        if (!ChatManager.Instance.isChatFocused)
        {

            if (Input.GetKey(KeyCode.W))
            {
                moveY = 1f;
                // transform.position += new Vector3(0, +1);
            }

            if (Input.GetKey(KeyCode.S))
            {
                moveY = -1f;
                // transform.position += new Vector3(0, -1);
            }
            if (Input.GetKey(KeyCode.A))
            {
                spriteFlip.x = 1;
                moveX = -1f;
                // transform.position += new Vector3(-1, 0);
            }

            if (Input.GetKey(KeyCode.D))
            {
                spriteFlip.x = -1;
                moveX = 1f;
                // transform.position += new Vector3(+1, 0);
            }

            moveDir = new Vector3(moveX, moveY).normalized;//normalized hace que la velocidad no aumente al ir en diagonal. W+A por ejemplo doblaria la speed
        }
    }
    private void FixedUpdate()
    {
       body2D.velocity = moveDir * moveSpeed;
     
    }
}

