﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
   public GameObject effectColisision;
   public HunterHP hunter;
    private Transform shootPosition;
        private Quaternion shootRotation;


    public void Start()
    {
        Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();
        transform.rotation = shootRotation;
        rb.AddForce(shootPosition.right * 10f, ForceMode2D.Impulse);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject auxEffect  = Instantiate(effectColisision, transform.position, Quaternion.identity);
        if (collision.transform.tag == "Hider")
        {//diferente color al dar a un hider y este pierde vida
            HiderHP hider = collision.transform.GetComponent<HiderHP>();
            hider.HealthDown(10);
            Destroy(gameObject);
            auxEffect.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0, 1); 
        }//si golpea a cualquier cosa que no sea un hider pierde vida 
        else if(collision.transform.tag == "prop")
         {
            hunter.HealthDown(10);
        }
         Destroy(auxEffect, 0.3f);
         Destroy(gameObject);
        hunter.GetComponentInParent<BoxCollider2D>().isTrigger = true;
    }

    public void SetLayer(GameObject hunter)
    {
        gameObject.layer = hunter.gameObject.layer;
    }
    public void SetPosition(Transform shootpos, Quaternion shootrotation)
    {
        shootRotation = shootrotation;
        shootPosition = shootpos;
    }

    public void SetHunter(GameObject hunterParent)
    {
        hunter = hunterParent.GetComponent<HunterHP>();
        // gameObject.transform.parent = hunter.gameObject.transform;
    }
    private void Update()
    {
        //transform.position = shootPosition.right + transform.forward * 10f;
        Destroy(gameObject, 1f);
    }
}
 
    