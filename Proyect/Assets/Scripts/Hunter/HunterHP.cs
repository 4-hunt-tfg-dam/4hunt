﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HunterHP : MonoBehaviour
{
    private PhotonView PV;
    public int health = 100;
    public int countAux;
    public HealthBar healthBar;
    public GameObject AimObject;
    public GameObject deadText;


    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }
    //public GameObject effectDeath; //todavia no tenemos este efecto creado.
    public void HealthDown (int damage) //public para poder llamar al metodo desde PlayerAimGun
    {
        health = health-damage;
        healthBar.SetHealth(health);

        if (health <= 0)
        {
            PV.RPC("PlayerDead",RpcTarget.All);
        }
    }

   [PunRPC] void PlayerDead()
    {

        countAux = RoomManager.Instance.HunterDeaths++;
        AimObject.SetActive(false);
        Animator deathAnimation = gameObject.GetComponent<Animator>();

        if (deathAnimation)
            deathAnimation.SetTrigger("Dead");

        StartCoroutine(Die(5f));
        //  StartCoroutine(Die(deathAnimation.GetCurrentAnimatorStateInfo(0).length));
    }
    IEnumerator Die(float delay )
    {
        deadText.SetActive(true);
        yield return new WaitForSeconds(delay);

        Destroy(gameObject);
    }
}
