﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class HunterMovement : MonoBehaviour
{

 //   [SerializeField] GameObject PlayerCamera;

    private PhotonView PV;
    private float moveSpeed = 8f;
    private Vector3 moveDir;
    private Rigidbody2D body2D;
    public Animator myAnim;
    public TMP_InputField ChatInputField;


    private void Awake()
    {
       body2D = GetComponent<Rigidbody2D>();
       PV = GetComponent<PhotonView>();
    }

    // Update is called once per frame
   private void Update()
    {
        if (!PV.IsMine)
            return;
        Vector3 spriteFlip = transform.localScale;
       // PlayerCamera.SetActive(true);
        //Optimizacion para implementarle la direccion al rigidbody
        float moveX = 0f;
        float moveY = 0f;
        if (!ChatInputField.isFocused)
        {
            if (Input.GetKey(KeyCode.W))
            {
                moveY = 1f;
                // transform.position += new Vector3(0, +1);
            }

            if (Input.GetKey(KeyCode.S))
            {
                moveY = -1f;
                // transform.position += new Vector3(0, -1);
            }
            if (Input.GetKey(KeyCode.A))
            {
                moveX = -1f;
                // transform.position += new Vector3(-1, 0);
            }

            if (Input.GetKey(KeyCode.D))
            {
                moveX = 1f;
                // transform.position += new Vector3(+1, 0);
            }
        }
        transform.localScale = spriteFlip;
        moveDir = new Vector3(moveX, moveY).normalized;//normalized hace que la velocidad no aumente al ir en diagonal. W+A por ejemplo doblaria la speed

       if(moveDir != Vector3.zero)
        {
            myAnim.SetBool("isWalking", true);
        }
        else
        {
            myAnim.SetBool("isWalking", false);
        }
    }
    private void FixedUpdate()
    {
       body2D.velocity = moveDir * moveSpeed;
     
    }
}

