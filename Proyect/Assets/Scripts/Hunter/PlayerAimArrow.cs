﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using TMPro;

public class PlayerAimArrow : MonoBehaviour
{

    public Animator playerRotate;
    public Transform hunter;
    public Transform shootPosition;
    public Texture2D crosshair;
    public HealthBar healthBar;
    public Camera playerCam;
    public GameObject Canvas;
    public TMP_Text PlayerNameText;
    public int myTeam;
    public BoxCollider2D hunterCollider;
    public GameObject bulletPrefab;


    private Transform aimTransform;
    private float FireRate = 2f;
    private float nextShot;
    private bool isReloading;
    private int currentAmmo = 1;
    private int hunterHealth = 100;
    private int currentHealth;
    private PhotonView PV;

    private void Awake()
    {
        currentHealth = hunterHealth;
        healthBar.SetMax(currentHealth);

        PV = GetComponent<PhotonView>();
        aimTransform = transform.Find("Aim");
        if (PV.IsMine)
        {
            Cursor.SetCursor(crosshair, new Vector2(crosshair.width/2, crosshair.height /2), CursorMode.ForceSoftware);
            myTeam = PlayerManager.Instance.myTeam;
           // PlayerNameText.text = PhotonNetwork.NickName;
            Canvas.SetActive(true);
            playerCam.gameObject.SetActive(true);

        }
        else if (myTeam == 1)
        {
            PlayerNameText.color = Color.cyan;
            PlayerNameText.text = PV.Owner.NickName;
        }
    }

    private void Update()
    {
        if (!PV.IsMine) return;

        healthBar.setAmmo(currentAmmo);

        Vector3 difference = GetMousePosition() - hunter.position;
        if (isReloading)
            return;

        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }

        if (Input.GetMouseButton(0) && Time.time > nextShot && difference.magnitude > 3.5 && !isReloading)
        {

            nextShot = Time.time + 1 / FireRate;
            PV.RPC("Shoot", RpcTarget.All, shootPosition.position, shootPosition.rotation);
            playerRotate.SetBool("Shoot", true);

           // Shoot();
            //esperar X segundos
            //  nextShot = Time.time + FireRate;
            //disparar de 3 en 3
        }
        else if(Time.time > nextShot){
            playerRotate.SetBool("Shoot", false);
            hunterCollider.isTrigger = false;

        }
        //calcular distancia entre el crosshair y el hunter para no poder dispararte a ti mismo
        if (difference.magnitude > 3.5)
        {
            Cursor.visible = true;
        }
        else
        {
            Cursor.visible = false;
        }
    }


[PunRPC] void Shoot(Vector3 pos, Quaternion dir)
    {
        hunterCollider.isTrigger = true;

        Vector3 pointing = GetMousePosition();
        Vector3 aimDirection = (pointing - transform.position).normalized;
        float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        aimTransform.eulerAngles = new Vector3(0, 0, angle);

        //rotacion izquierda/derecha del objeto aim
        Vector3 rotation = Vector3.one;
        if (angle > 90 || angle < -90)
        {
            rotation.y = -1f;
        }
        else
        {
            rotation.y = +1f;
        }
        aimTransform.localScale = rotation;

       

        if (angle > 40 && angle < 130)
        {//Apuntando hacia arriba
            playerRotate.SetFloat("HorizontalShoot", -0);
            playerRotate.SetFloat("VerticalShoot", 1);

        }
        else if (angle > -130 && angle < -40)
        {//Apuntando hacia abajo
            playerRotate.SetFloat("HorizontalShoot", -0);
            playerRotate.SetFloat("VerticalShoot", -1);

        }
        else if (angle < 130 && angle < -130 || angle > 130 && angle > -130)
        {//Apuntando hacia izquierda
            playerRotate.SetFloat("HorizontalShoot", -1);
            playerRotate.SetFloat("VerticalShoot", 0);
        }
        else if (angle > -40 && angle < 40)
        {//Apuntando hacia derecha
            playerRotate.SetFloat("HorizontalShoot", 1);
            playerRotate.SetFloat("VerticalShoot", 0);
        }
        healthBar.setAmmo(currentAmmo);
        currentAmmo--;

        GameObject bullet = Instantiate(bulletPrefab, shootPosition.position, shootPosition.rotation);
        //GameObject bullet = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/AntiqueMap", "BulletPrefab"), shootPosition.position, shootPosition.rotation);


        bullet.GetComponent<Bullet>().SetHunter(gameObject);
        bullet.GetComponent<Bullet>().SetLayer(gameObject);
        bullet.GetComponent<Bullet>().SetPosition(shootPosition, shootPosition.rotation);

        //bullet.layer = hunter.gameObject.layer; //Se actualiza el layer con el del hunter
        //Aqui digo que las balas generadas sean Child del hunter que las lanza
        //Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        //rb.AddForce(shootPosition.right * 10f, ForceMode2D.Impulse);
    }

    IEnumerator Reload() 
    {
        isReloading = true;

        //bowAnimator.SetTrigger("Reload");
        yield return new WaitForSeconds(1f);
        currentAmmo = 1;
        isReloading = false;
    }



    //Coger la posicion y la rotacion Z que tiene el cursor
    public static Vector3 GetMousePosition()
    {
        Vector3 vec = GetMousePositionZ(Input.mousePosition, Camera.main);
        vec.z = 0f;
        return vec;
    }

    public static Vector3 GetMousePositionZ()
    {
        return GetMousePositionZ(Input.mousePosition, Camera.main);
    }

    public static Vector3 GetMousePositionZ(Camera worldCamera)
    {
        return GetMousePositionZ(Input.mousePosition, worldCamera);
    }

    public static Vector3 GetMousePositionZ(Vector3 screenPosition, Camera worldCamera)
    {
        Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }
}
