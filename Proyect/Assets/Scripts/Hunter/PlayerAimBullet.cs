﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using TMPro;

public class PlayerAimBullet : MonoBehaviour
{
    
    
    public Animator gunAnimator;
    public Animator playerRotate;
    public Transform hunter;
    public Transform shootPosition;
    public GameObject shootEffect;
    public Texture2D crosshair;
    public HealthBar healthBar;
    public Camera playerCam;
    public GameObject Canvas;
    public TMP_Text PlayerNameText;
    public int myTeam;
    public TMP_InputField ChatInputField;



    private Transform aimTransform;
    private float FireRate = 2f;
    private float nextShot;
    private bool isReloading;
    private int currentAmmo = 3;
    private int hunterHealth = 100;
    private int currentHealth;
    private PhotonView PV;


    private void Awake()
    {
        currentHealth = hunterHealth;
        healthBar.SetMax(currentHealth);
        PV = GetComponent<PhotonView>();
        aimTransform = transform.Find("Aim");
        if (PV.IsMine)
        {
            Cursor.SetCursor(crosshair, Vector2.zero, CursorMode.ForceSoftware);
            myTeam = PlayerManager.Instance.myTeam;
            PlayerNameText.text = PhotonNetwork.NickName;
            Canvas.SetActive(true);
            playerCam.gameObject.SetActive(true);

        }else if(myTeam == 1)
        {
            PlayerNameText.color = Color.cyan;
            PlayerNameText.text = PV.Owner.NickName;
        }
    }

    private void Update()
    {
        if (!PV.IsMine) return;

        // healthBar.SetHealth(currentHealth);
        healthBar.setAmmo(currentAmmo);
        Vector3 difference = GetMousePosition() - hunter.position;
        if (isReloading)
            return;

        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }

       Aim();
 
            if (Input.GetMouseButton(0) && Time.time > nextShot && difference.magnitude > 3.5 && !isReloading)
            {
                // PV.RPC("Shoot", RpcTarget.OthersBuffered, shootPosition.position, shootPosition.rotation);
                nextShot = Time.time + 1 / FireRate;
                PV.RPC("Shoot", RpcTarget.All);

                //Shoot(shootPosition.position, shootPosition.rotation);
                //esperar X segundos
                //  nextShot = Time.time + FireRate;
                //disparar de 3 en 3
            }
        

       
        //calcular distancia entre el crosshair y el hunter para no poder dispararte a ti mismo
        if (difference.magnitude > 3.5)
        {
            // float distance = 10;
            //crosshair.transform.position = new Vector3(pointing.x, pointing.y, -1) * distance;
         /*   crosshair.SetActive(true);
            crosshair.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);*/
            Cursor.visible = true;
        }
        else
        {
            Cursor.visible = false;
            //crosshair.SetActive(true);
        }
    }


   // [PunRPC]
    void Aim()
    {

        Vector3 pointing = GetMousePosition();

        Vector3 aimDirection = (pointing - transform.position).normalized;
        float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        aimTransform.eulerAngles = new Vector3(0, 0, angle);

        //Forma de ver que valores te esta devolviendo y comprobar que lo has realizado correctamente 
        //Debug.Log(pointing);


        //rotacion izquierda/derecha del objeto aim
        Vector3 rotation = Vector3.one;
        if(angle > 90 || angle < -90)
        {
            rotation.y = -1f;
        }else{
            rotation.y = +1f;
        }
        aimTransform.localScale = rotation;

        playerRotate.SetBool("Aiming", true);//Condicion para animator


        if (angle > 40 && angle < 130)
        {//Apuntando hacia arriba
            playerRotate.SetFloat("Horizontal", -0);
            playerRotate.SetFloat("Vertical", 1);
            
        }
        else if (angle > -130 && angle < -40)
        {//Apuntando hacia abajo
            playerRotate.SetFloat("Horizontal", -0);
            playerRotate.SetFloat("Vertical", -1);

        } else if (angle < 130 && angle < -130 || angle > 130 && angle > -130)
        {//Apuntando hacia izquierda
            playerRotate.SetFloat("Horizontal", -1);
            playerRotate.SetFloat("Vertical", 0);
        } else if (angle > -40 && angle < 40)
        {//Apuntando hacia derecha
            playerRotate.SetFloat("Horizontal", 1);
            playerRotate.SetFloat("Vertical", 0);
        }
        else
        {
            playerRotate.SetBool("Aiming", false);
        }
    }

    [PunRPC]
    void Shoot(/*Vector3 pos, Quaternion dir*/)
    {
      
            healthBar.setAmmo(currentAmmo);
            currentAmmo--;

            //healthBar.SetHealth(currentHealth);

            gunAnimator.SetTrigger("Recoil");
            // Instantiate(shootEffect, aimTransform);

            GameObject bullet = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/MapAliens", "BulletPrefab"), shootPosition.position, shootPosition.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(shootPosition.right * 50f, ForceMode2D.Impulse);

            /* if (PV.IsMine)
             {
             GameObject bullet = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/MapAliens", "BulletPrefab"), pos, dir);
                 PV.RPC("Shoot", RpcTarget.OthersBuffered, shootPosition.position, shootPosition.rotation);
                 GameObject bullet = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "BulletPrefab"), shootPosition.position, shootPosition.rotation);
                 Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                 rb.AddForce(shootPosition.right * 50f, ForceMode2D.Impulse);
             }*/
            // PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "HunterManager"), Vector3.zero, Quaternion.identity);

            // DestroyImmediate(shootEffect);
        
    }
    IEnumerator Reload()
    {
        isReloading = true;
       
        gunAnimator.SetTrigger("Reload");
        yield return new WaitForSeconds(0.3f);

        currentAmmo = 3;
        isReloading = false;
    }



    //Coger la posicion y la rotacion Z que tiene el cursor
    public static Vector3 GetMousePosition()
    {
        Vector3 vec = GetMousePositionZ(Input.mousePosition, Camera.main);
        vec.z = 0f;
        return vec;
    }

    public static Vector3 GetMousePositionZ()
    {
        return GetMousePositionZ(Input.mousePosition, Camera.main);
    }

    public static Vector3 GetMousePositionZ(Camera worldCamera)
    {
        return GetMousePositionZ(Input.mousePosition, worldCamera);
    }

    public static Vector3 GetMousePositionZ(Vector3 screenPosition, Camera worldCamera)
    {
        Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }
}
