﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Cainos.PixelArtTopDown_Basic
{
    public class TopDownCharacterController : MonoBehaviour
    {
        public float speed; //Velocidad de movimiento
        private PhotonView PV;
        private Rigidbody2D body2D;
        public Animator animator;
        Vector2 move; //Movimiento sobre un vector xy "horizontal-vertical"
       
        

        private void Awake()
        {
            body2D = GetComponent<Rigidbody2D>();
            PV = GetComponent<PhotonView>();
            
        }

        private void Update()
        {
            //Parte del photonview para el canvas
            if (!PV.IsMine) return;
            //Parte en la que se dice hacia donde te mueves con input
            if (!ChatManager.Instance.isChatFocused)
            {
                move.x = Input.GetAxisRaw("Horizontal");//Izquierda = -1 movimiento y Derecha = 1 sobre movimiento
                move.y = Input.GetAxisRaw("Vertical");//Abajo = -1 y Arriba = 1



                animator.SetFloat("Horizontal", move.x);
                animator.SetFloat("Vertical", move.y);
                animator.SetFloat("Speed", move.sqrMagnitude);//La longitud del vector en el que te mueves es decir la velocidad
            }
        }

        private void FixedUpdate()
        {
            //Parte en la que se ejecuta el movimiento
            body2D.MovePosition(body2D.position + move.normalized * speed * Time.fixedDeltaTime);//Funcion que hace que se mueva el objeto de la zona en la que esta
                                                                                                 //a la velocidad indicada y solo se multiplica por constante de tiempo para que tenga un movimiento regular
                                                                                                 //para que se mantenga la velocidad y no aumente cada vez que lo pulsamos       
        }
    }
}
