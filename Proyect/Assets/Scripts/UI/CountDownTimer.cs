﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountDownTimer : MonoBehaviour
{

    public int countdownTime;
    public GameObject BlackScreen;
    public TMP_Text text;
    public GameObject hunter;
    void Start() {
     
        StartCoroutine(CountdownToStart());
    }
    IEnumerator CountdownToStart()
    {
      while(countdownTime > 0)
        {
            text.text = "Hiders have " + countdownTime.ToString() + " seconds to hide";

            yield return new WaitForSeconds(1f);

            countdownTime--;
        }

            text.text = "HUNT TIME!";
        yield return new WaitForSeconds(1f);

        //Habilitar movimiento y disparos 
        BlackScreen.SetActive(false);
        hunter.GetComponent<PlayerAimArrow>().enabled = true;
        hunter.GetComponent<Cainos.PixelArtTopDown_Basic.TopDownCharacterController>().enabled = true; 
      

    }
    }
 



