﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorChanger : MonoBehaviour
{
    // Start is called before the first frame update
    private SpriteRenderer rend;
    public Texture2D hand;
    public Texture2D handClick;
    void Start()
    {
        /*Cursor.visible = false;
        rend = GetComponent<SpriteRenderer>();*/
        Cursor.SetCursor(hand, new Vector2(hand.width / 2, hand.height / 2), CursorMode.ForceSoftware);
    }

    // Update is called once per frame
    void Update()
    { 
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(MyCoroutine());
        }
    }

    IEnumerator MyCoroutine()
    {
        //This is a coroutine
        Cursor.SetCursor(handClick, new Vector2(handClick.width / 2, handClick.height / 2), CursorMode.ForceSoftware);
        yield return new WaitForSeconds(0.3f);    //Wait 2 frame
        Cursor.SetCursor(hand, new Vector2(hand.width / 2, hand.height / 2), CursorMode.ForceSoftware);
    }
}
