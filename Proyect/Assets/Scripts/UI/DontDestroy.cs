﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    public static DontDestroy estadoJuego;

    private void Awake()
    {
        if(estadoJuego==null)//indica que es la primera vez que se invoca script
        {
            estadoJuego = this;
            DontDestroyOnLoad(gameObject);
        }else if(estadoJuego != null)
        {
            Destroy(gameObject);
        }
       
    }
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
