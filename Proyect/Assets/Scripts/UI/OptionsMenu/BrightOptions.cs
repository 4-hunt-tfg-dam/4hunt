﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrightOptions : MonoBehaviour
{
    public Slider slider;
    public float sliderValue;
    public Image panelBrillo;

    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("brillo", 0f);//guardar valor predeterminado

        panelBrillo.color = new Color(panelBrillo.color.r, panelBrillo.color.g, panelBrillo.color.b, slider.value);//la transparencia igual al slidervalue y color se mantiene
    }

   
    void Update()
    {
        
    }

    public void ChangeSlider(float valor)//Obtiene el valor del slidervalue y lo coloca en variable brillo para guardarlo al seleccionarlo en inspector de slider
    {
        sliderValue = valor;
        PlayerPrefs.SetFloat("brillo", sliderValue);
        panelBrillo.color = new Color(panelBrillo.color.r, panelBrillo.color.g, panelBrillo.color.b, slider.value);
    }

}
