﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FullScreenOptions : MonoBehaviour
{
    public Toggle toggle;

    public TMP_Dropdown resolutionsDropdown;
    Resolution[] resolutions;//array con todas las resoluciones.

    void Start()
    {
        if (Screen.fullScreen)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }

        CheckResolution();
    }

    
    void Update()
    {
        
    }

    public void FullScreenActivate(bool pantallaCompleta)
    {
        Screen.fullScreen = pantallaCompleta; 
    }

    public void CheckResolution()
    {
        resolutions = Screen.resolutions; //busca las resoluciones del pc
        resolutionsDropdown.ClearOptions();//borra las anteriores y coloca las buenas
        List<string> options = new List<string>();
        int actualResoution = 0;

        for(int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);//coloca las opciones con el for en el dropdown

            if (Screen.fullScreen && resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)//comprueba la resolucion para mostrarla
            {
                actualResoution = i;//guarda resolucion actual al buscarla en la pantalla del pc    
            } 
        }

        resolutionsDropdown.AddOptions(options);
        resolutionsDropdown.value = actualResoution;//colocar todo en pantalla
        resolutionsDropdown.RefreshShownValue();

        resolutionsDropdown.value = PlayerPrefs.GetInt("resolutionNumber", 0);//comienza como 0 y se establece el primer cambio realizado


    }

    public void ChangeResolution(int resolutionIndex)
    {
        PlayerPrefs.SetInt("resolutionNumber", resolutionsDropdown.value);// guarde resolucion al cerrar juego

        Resolution resolution = resolutions[resolutionIndex];//busca la resolucion en el indice
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);//y coloca la resolucion y boobleano para detectar si es pantalla completa
    }
}
