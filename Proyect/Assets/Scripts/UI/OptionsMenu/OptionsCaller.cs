﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsCaller : MonoBehaviour
{
    public OptionsController panelOpciones;//variable publica que hace referencia a otro script
    
    void Start()
    {
        panelOpciones = GameObject.FindGameObjectWithTag("Opciones").GetComponent<OptionsController>();//buscar el componente por tag y lo muestra
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    MostrarOpciones();
        //}
    }

    public void MostrarOpciones()
    {
        panelOpciones.pantallaOpciones.SetActive(true);
    }
}
