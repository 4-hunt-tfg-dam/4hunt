﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenesObjects : MonoBehaviour
{
    private void Awake()
    {
        var noDestruirEntreEscenas = FindObjectsOfType<ScenesObjects>(); //guardas el objeto en la variable
        if(noDestruirEntreEscenas.Length > 1)//si en una escena encuentra este objeto lo destruye
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
