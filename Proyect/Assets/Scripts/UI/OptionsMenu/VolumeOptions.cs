﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //libreria que habilita cambiar canvas

public class VolumeOptions : MonoBehaviour
{
    public Slider slider;
    public float sliderValue;
    public Image imagenMute;
  
    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("volumenAudio", 0.5f); //crear valor para guardar posicion slider despues de cerrar menu y predefinido a 0.5 de volumen
        AudioListener.volume = slider.value;
        PlayerMute();
        DontDestroyOnLoad(gameObject);
    }

    public void ChangeSlider(float valor)//Obtiene el valor del slidervalue y lo coloca en variable volumenAudio para guardarlo al seleccionarlo en inspector de slider
    {
        sliderValue = valor;
        PlayerPrefs.SetFloat("volumenAudio", sliderValue);
        AudioListener.volume = slider.value;
        PlayerMute();
    }

    public void PlayerMute() //Crea un icono al encontrarte muteado mas adelante
    {
        if(sliderValue == 0)
        {
            imagenMute.enabled = true;
        }

        else
        {
            imagenMute.enabled = false;
        }
    }
}
