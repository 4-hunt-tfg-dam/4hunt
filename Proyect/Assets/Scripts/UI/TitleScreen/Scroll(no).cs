﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolll : MonoBehaviour
{

    //script para movimiento de fondo en pantalla de inicio
    private float length, posIni;
    public float move;
    public GameObject cam;
    
    void Start()
    {
        posIni = transform.position.x;//posicion x qur es la inicial de cada uno
        length = GetComponent<SpriteRenderer>().bounds.size.x; //la longitud del sprite
    }

    
    void Update()
    {
        float repe = (cam.transform.position.x * (1 - move)); //para marcar hasta que punto se mueve la camara se pone el contenido del parentesisi por ser relativo a al camara
        float distance = (cam.transform.position.x * move); //indica la distancia que se ha movido realizando el efecto

        transform.position = new Vector3(posIni + distance, transform.position.y, transform.position.z);// Mover la camara con la posicion inicial mas distancia

        //si es mas grande vuelve a empezar, si no, sigue
        if (repe > posIni + length)
        {
            posIni += length;
        }

        else if (repe < posIni - length)
        {
            posIni -= length;
        }
    }
}
