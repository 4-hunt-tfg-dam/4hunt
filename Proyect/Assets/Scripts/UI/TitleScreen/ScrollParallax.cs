﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScrollParallax : MonoBehaviour
{

    public bool IniciarEnMovimiento = true;
    public GameObject fondo;
    public float velocidad = 0f;
    private bool enMovimiento = false;
    private float tiempoInicio = 0f;

    // Use this for initialization
    void Start()
    {

        //NotificationCenter.DefaultCenter().AddObserver(this, "PersonajeEmpiezaACorrer");
        //NotificationCenter.DefaultCenter().AddObserver(this, "PersonajeHaMuerto");
        if (IniciarEnMovimiento)
        {
            enMovimiento = true;
        }
    }

    //void PersonajeHaMuerto()
    //{
    //    enMovimiento = false;
    //}

    //void PersonajeEmpiezaACorrer()
    //{
    //    enMovimiento = true;
    //    tiempoInicio = Time.time;
    //}

    // Update is called once per frame
    void Update()
    {
        if (enMovimiento)
        {
            fondo.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(((Time.time - tiempoInicio) * velocidad) , 0);
           
        }
    }
}

