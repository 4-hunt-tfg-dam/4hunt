﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


//Cuando algo se posiciona sobre el altar se ilumina mediante un trigger
namespace Cainos.PixelArtTopDown_Basic
{
 
    public class PropsAltar : MonoBehaviour
    {
        private PhotonView PV;
        public List<SpriteRenderer> runas;
        public float velocidadIluminacion;
        public GameObject highlight;
        public bool interactable = false;
        private Color progresoColor;
        private Color colorObjetivo;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Hider")
            {
                other.GetComponent<Transformacion>().rc = gameObject;
                colorObjetivo = new Color(1, 1, 1, 0);
                interactable = true;
                highlight.SetActive(true);

            }
            if(gameObject.transform.tag == "Escape" && other.tag == "Hider")
            {
                Escape(other.gameObject);
            }
        }

        private void Escape(GameObject other)
        {
            Debug.Log("Llamada a escapaaar");
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == "Hider")
            {
                highlight.SetActive(false);
                colorObjetivo = new Color(1, 1, 1, 0);
                interactable = false;
            }    
        }

        private void Update()
        {
            progresoColor = Color.Lerp(progresoColor, colorObjetivo, velocidadIluminacion * Time.deltaTime);
           
            foreach (var r in runas)
            {
                r.color = progresoColor;
            }
        }
    }
}
