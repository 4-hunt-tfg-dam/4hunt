﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Cuando algo se posiciona sobre el altar se ilumina mediante un trigger
namespace Cainos.PixelArtTopDown_Basic
{

    public class PropsAltar : MonoBehaviour
    {
        public List<SpriteRenderer> runas;
        public float velocidadIluminacion;

        private Color progresoColor;
        private Color colorObjetivo;

        private void OnTriggerEnter2D(Collider2D other)
        {
            colorObjetivo = new Color(1, 1, 1, 1);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            colorObjetivo = new Color(1, 1, 1, 0);
        }

        private void Update()
        {
            progresoColor = Color.Lerp(progresoColor, colorObjetivo, velocidadIluminacion * Time.deltaTime);

            foreach (var r in runas)
            {
                r.color = progresoColor;
            }
        }
    }
}
