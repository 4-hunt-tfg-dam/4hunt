﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
using ExitGames.Client.Photon;


[System.Serializable]
public class mapData
{
    public int mapScene;
    public string mapName;
}
public class MenuController : MonoBehaviourPunCallbacks
{

    public static MenuController Instance;
   // [SerializeField] private string VersionName = "0.1";
    [SerializeField] private GameObject StartMenu;
    [SerializeField]
    private GameObject LoadingMenu;
    [SerializeField] private TMP_InputField UsernameInput;
    [SerializeField] private TMP_InputField CreateGameInput;

    
    //Botones
    [SerializeField] private Button HostButton;
    [SerializeField] private Button StartButton;
    [SerializeField] private TMP_Text ReadyButton;
    [SerializeField] private GameObject startGameButton;

    //Menus
    [SerializeField] private GameObject Lobby;
    [SerializeField] private GameObject GameMenu;
    

    //Contenedores y Items de las listas
    [SerializeField] private TMP_Text LobbyName;
    [SerializeField] private GameObject lobbyNamePrefab;
    [SerializeField] private Transform lobbyContent;
    [SerializeField] private GameObject roomListItemPrefab;
    [SerializeField] private Transform roomListContent;

    

    [SerializeField] private mapData[] maps;
    [SerializeField] private TMP_Text mapValue;
    [SerializeField] private GameObject mapButton;

    private int currentmap;
    private Player PlayerConnected;
    private Text Username;
  

    private void Awake()
    {
        Instance = this;
        PhotonNetwork.ConnectUsingSettings();

    }

    public void ChangeMap()
    {
        currentmap++;
        if (currentmap >= maps.Length) currentmap = 0;
        mapValue.text = "MAP: " + maps[currentmap].mapName.ToUpper();
    }

   public override void OnConnectedToMaster()
    {
       //is a callback called by Photon when we successfully connect to the master server
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        PhotonNetwork.AutomaticallySyncScene = true; //Todos los jugadores estaran en la misma escena.
    }

    public void ChangeUserNameInput()
    {
      
        if (UsernameInput.text.Length >= 3 && UsernameInput.text.Length <= 9) 
        {
            StartButton.interactable = true;
        }
        else
        {
            StartButton.interactable = false;  
        }
    }


    public void SetUserName()
    {
        PhotonNetwork.NickName = UsernameInput.text;
        Debug.Log("Tienes"+PhotonNetwork.NickName);
    }
   public void ChangeRoomInput()
    {
        if (CreateGameInput.text.Length >= 3 && CreateGameInput.text.Length <= 9)
        {
            HostButton.interactable = true;
        }
        else
        {
            HostButton.interactable = false;
        }
    }

    public void changeReady()
    {
        bool isReady = lobbyNamePrefab.GetComponent<PlayerLobbyController>().getIfReady();

        if (isReady)
        {
            ReadyButton.text = "NOT READY";
        }
        else
        {
            ReadyButton.text = "READY";
        }
    }

    public void CreateGame()
    {
        if (string.IsNullOrEmpty(CreateGameInput.text))
        {
            return;
        }
        else
        {
            PhotonNetwork.CreateRoom(CreateGameInput.text, new RoomOptions() { MaxPlayers = 5 }, null);

        }
    }
    public void JoinGame(RoomInfo info)
    {
        CreateGameInput.text = "";
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.BroadcastPropsChangeToAll = true;
        roomOptions.MaxPlayers = 5;

        PhotonNetwork.JoinRoom(info.Name);
    }
  
    public override void OnCreatedRoom()
    {
        ChangeMap(); // poner mapa predeterminado
        LobbyName.text = PhotonNetwork.CurrentRoom.Name;
        startGameButton.SetActive(PhotonNetwork.IsMasterClient);
        mapButton.SetActive(PhotonNetwork.IsMasterClient);
        Lobby.SetActive(true);
        GameMenu.SetActive(false);
        PhotonNetwork.NickName = UsernameInput.text;
    }

    public override void OnJoinedRoom()
    {
        LobbyName.text = PhotonNetwork.CurrentRoom.Name;
        PhotonNetwork.NickName = UsernameInput.text;

        Lobby.SetActive(true);
        GameMenu.SetActive(false);
        
        Player[] players = PhotonNetwork.PlayerList;

        foreach(Transform child in lobbyContent)
        {
            Destroy(child.gameObject);
        }
        
        for (int i = 0; i < players.Count(); i++)
        {
            Instantiate(lobbyNamePrefab, lobbyContent).GetComponent<PlayerLobbyController>().SetUp(players[i]);
        }
    }


    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        startGameButton.SetActive(true);
        mapButton.SetActive(true);
        /*startGameButton.SetActive(PhotonNetwork.IsMasterClient);
        mapButton.SetActive(PhotonNetwork.IsMasterClient);*/
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        //  errorText.text = "Room Creation Failed: " + message;
        return;
    }

    public void LeaveLobby()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect(); //Need to disconnect and Reconnect because of a Bug that was closing the game
        PhotonNetwork.ConnectUsingSettings();
        StartMenu.SetActive(false);
        GameMenu.SetActive(true);
        startGameButton.SetActive(false);
        mapButton.SetActive(false);
    }

    public void LeaveLobbyInGame()
    {
       /* PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect(); //Need to disconnect and Reconnect because of a Bug that was closing the game
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.LoadLevel(0);*/
        SceneManager.LoadScene(0);
        Cursor.visible = true;

        LeaveLobby(); //PRUEBA
    }
    void OnPhotonPlayerConnected(Player otherPlayer)
    {
        OnPlayerEnteredRoom(otherPlayer);
    }

   public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        
        foreach (Transform trans in roomListContent)
        {
            Destroy(trans.gameObject);
        }
            for (int i = 0; i < roomList.Count; i++)
            {
            if (roomList[i].RemovedFromList)
                continue;
                Instantiate(roomListItemPrefab, roomListContent).GetComponent<RoomListController>().SetUp(roomList[i]);
       
        }
        
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        /* lobbyNamePrefab.GetComponent<PlayerLobbyController>().OnReadyForGame();*/
        Instantiate(lobbyNamePrefab, lobbyContent).GetComponent<PlayerLobbyController>().SetUp(newPlayer);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        bool[] readyPlayer = new bool[10];
        bool allReady = true;
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        Player[] players = PhotonNetwork.PlayerList;
        for (int i = 0; i < players.Count(); i++)
        {
            readyPlayer[i] = lobbyNamePrefab.GetComponent<PlayerLobbyController>().getPlayerReady(players[i]);
            Debug.Log(players[i] + readyPlayer.ToString());

            if (!allReady) break;
            else allReady = readyPlayer[i];
        }
        if (allReady) //&& players.Count()==5)
        {
            startGameButton.gameObject.GetComponent<Button>().interactable = true;
        }
        else
            startGameButton.gameObject.GetComponent<Button>().interactable = false;
    }
     
    public void StartGame()
    {//meter pantalla de carga 
        if(maps[currentmap].mapScene == 1) PhotonNetwork.LoadLevel(2); //Index 2 = Game Scene
        if (maps[currentmap].mapScene == 2) PhotonNetwork.LoadLevel(3);
         
    }



}



