﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using TMPro;
using Photon.Pun;

public class RoomListController: MonoBehaviour
{

    [SerializeField] TMP_Text textName;

    RoomInfo info;
   public void SetUp(RoomInfo newRoomInfo)
    {
        info = newRoomInfo;
        textName.text = newRoomInfo.Name +"   "+ newRoomInfo.PlayerCount +"/"+ newRoomInfo.MaxPlayers;
        
    }

    public void OnClick()
    {
        //PlayerLobbyController.Instance.OnReadyForGame();
        MenuController.Instance.JoinGame(info);
    }
}
