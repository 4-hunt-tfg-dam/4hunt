﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System.IO; //to get folders paths.
using TMPro;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class RoomManager : MonoBehaviourPunCallbacks, IOnEventCallback
{

    public Transform[] spawnpoints;
    public Transform[] propSpawns;
    public string[] propPrefabs;
    public static RoomManager Instance;
    public int nextPlayersTeam;
    public int HiderDeaths = 0;
    public int HunterDeaths = 0;
    public int HiderScaped = 0;
    public TMP_Text pingText;
    public GameObject EscapeMenu;
    public GameObject PlayerPopUp;
    public GameObject PopUpPanel;
    public GameObject huntersWinScreen;
    public GameObject hidersWinScreen;
    public GameObject EndGameCanvas;
    private bool Off = false;
    private PhotonView PV;
    public int sceneActual;


    //Timer Game Stuff
    public int matchLength = 180; //Total game seconds
    public TMP_Text timerCanvas;
    private int currentMatchTime;
    private Coroutine timerCoroutine;//Condition for decrement time

   public void Start()
    {
        PV = GetComponent<PhotonView>();
        InitializeTimer();
        if (Instance)//another RoomManager exists
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);//unique RoomManager
        Instance = this;
    }

    void Update()
    {
        //pv.ismine
         CheckInput(); //Constant check if Escape pressed
         pingText.text = "PING: " + PhotonNetwork.GetPing(); 
         RefreshTimerUI();
         CheckGameConditions(currentMatchTime, HiderDeaths, HunterDeaths);
        
    }
    #region PhotonEvents
    //PRUEBA EVENT CODES CON PHOTONNETWORK
    public enum EventCodes : byte
    {
        RefreshTimer
    }

    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code >= 200) return;

        EventCodes e = (EventCodes)photonEvent.Code;
        object[] o = (object[])photonEvent.CustomData;

        switch (e)
        {
            case EventCodes.RefreshTimer:
                RefreshTimer_R(o);
                break;
        }
    }
    #endregion
    private void CheckInput()
    {
        if(Off && Input.GetKeyDown(KeyCode.Escape))
        {
            EscapeMenu.SetActive(false);
            PopUpPanel.SetActive(true);
            timerCanvas.gameObject.SetActive(false);
            pingText.gameObject.SetActive(true);
            Off = false;
   
        }
        else if(!Off && Input.GetKeyDown(KeyCode.Escape))
        {
            EscapeMenu.SetActive(true);
            timerCanvas.gameObject.SetActive(false);
            pingText.gameObject.SetActive(false);
            PopUpPanel.SetActive(false);
            Off = true;
        }

        if(Input.GetKeyDown(KeyCode.A)|| Input.GetKeyDown(KeyCode.W)|| Input.GetKeyDown(KeyCode.D)|| Input.GetKeyDown(KeyCode.S))
        {//Si te mueves se cierra el menu
            EscapeMenu.SetActive(false);
            Off = true;
        }
    }
    public override void OnEnable()
    {
        base.OnEnable();
        SceneManager.sceneLoaded += OnSceneLoaded;
        PhotonNetwork.AddCallbackTarget(this);
    }
    public override void OnDisable()
    {
        base.OnDisable();
        SceneManager.sceneLoaded -= OnSceneLoaded;
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)//called everytime scenes change
    {
        if(scene.buildIndex == 2)//game Mapa 1
        {
            sceneActual = 1;
        for (int i = 0; i < propSpawns.Length; i++) //Spawn Objects
            {
                // Transform spawn = GetPropSpawns();
                Transform spawn = propSpawns[i].transform;
                string prop = GetPropPrefabs();
                //Se hace con path porque PhotonNetwork solo recibe strings, no prefabs como tal.      
                PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/AntiqueMap/Props", prop), spawn.position, spawn.rotation);
            }
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/MapAliens", "PlayerManager"), Vector3.zero, Quaternion.identity);
           // PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "HiderManager"), Vector3.zero, Quaternion.identity); 
        }else if(scene.buildIndex == 3)
        {
            sceneActual = 2;
            for (int i = 0; i < propSpawns.Length; i++) //Spawn Objects
            {
                // Transform spawn = GetPropSpawns();
                Transform spawn = propSpawns[i].transform;
                string prop = GetPropPrefabs();
                //Se hace con path porque PhotonNetwork solo recibe strings, no prefabs como tal.      
                PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/AntiqueMap/Props", prop), spawn.position, spawn.rotation);
            }
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/AntiqueMap", "PlayerManager"), Vector3.zero, Quaternion.identity);
        }
    }
    public void SetTeam()
    {
        if (nextPlayersTeam == 1)
        {
            nextPlayersTeam = 2;
        }
        else
            nextPlayersTeam = 1;
    }
    public Transform GetSpawnPoint(int team)
    {
        if(team == 1)
        {
            return spawnpoints[Random.Range(0, 2)].transform;
        }else
        {
            return spawnpoints[Random.Range(2, 5)].transform;
        }
    }

    public Transform GetPropSpawns()
    {
        return propSpawns[Random.Range(0, propSpawns.Length)].transform;
    }

    public string GetPropPrefabs()
    {
        return propPrefabs[Random.Range(0, propPrefabs.Length)];
    }

    public void LeaveRoom()
    {
        
        EscapeMenu.SetActive(false);
        EndGame();
        MenuController.Instance.LeaveLobbyInGame();
      
    }

    private void OnPhotonPlayerConnected(Player player)
    {
        GameObject aux = Instantiate(PlayerPopUp, new Vector2(0, 0), Quaternion.identity);
        aux.transform.SetParent(PopUpPanel.transform, false); //no se reescala con los mensajes.
        aux.GetComponent<TMP_Text>().text = player.NickName + "joined the game";
        aux.GetComponent<TMP_Text>().color = Color.green;
        Debug.Log("SIIIIII");
    }

    private void OnPhotonPlayerDisconnected(Player player)
    {
        GameObject aux = Instantiate(PlayerPopUp, new Vector2(0, 0), Quaternion.identity);
        aux.transform.SetParent(PopUpPanel.transform, false); //no se reescala con los mensajes.
        aux.GetComponent<TMP_Text>().text = player.NickName + "left the game";
        aux.GetComponent<TMP_Text>().color = Color.red;
        Debug.Log("NOOOOO");
    }


    //TIMER FUNCTIONS

        private void InitializeTimer()
    {
        currentMatchTime = matchLength;
        RefreshTimerUI();

        if (PhotonNetwork.IsMasterClient)
        {
            timerCoroutine = StartCoroutine(Timer());
         
        }
    }

    private IEnumerator Timer()//Only host, and send it to others Players Game
    {
        yield return new WaitForSeconds(1f);

        currentMatchTime -= 1;
        
        if(currentMatchTime <= 0)
        {
            timerCoroutine = null;
          //Cambiar estado del game--CondicionVictoria Hiders  UpdatePlayers_S((int)GameState.Ending, playerInfo);
        }
        else
        {
            RefreshTimer_S();
            timerCoroutine = StartCoroutine(Timer());
        }
    }
    private void RefreshTimerUI()
    {
        string minutes = (currentMatchTime / 60).ToString("00");
        string seconds = (currentMatchTime % 60).ToString("00");
        timerCanvas.text = $"{minutes}:{seconds}";
    }

    public void RefreshTimer_S()
    {
        object[] package = new object[] { currentMatchTime };
        Debug.Log("tIMER enviado");
        PhotonNetwork.RaiseEvent(
            (byte)EventCodes.RefreshTimer,
            package,
            new RaiseEventOptions { Receivers = ReceiverGroup.All },
            new SendOptions { Reliability = true }
    );
    }

    public void RefreshTimer_R(object[] data)
    {
        currentMatchTime = (int)data[0];
        RefreshTimerUI();

    }

    private void Escape()
    {

    }
    private void EndGame()
    {
        //añadir estados al Game? con el courutine

        if (timerCoroutine != null) StopCoroutine(timerCoroutine);
        currentMatchTime = 0;
        RefreshTimerUI();

        //disable room
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.DestroyAll();
            //mensaje de host
        }
    }
    private void CheckGameConditions(int gameTime, int hidersCount, int hunterCount)
    {

        // CONDICIONES VICTORIA HUNTERS
        if (hidersCount >= 3 || gameTime<= 0)
        {
            //EndGame y deshabilitar todo, menos WinCanvas +Añadir camara en el canvas?
            huntersWinScreen.SetActive(true);

        }

        //CONDICIONES VICTORIA HIDERS
        if(hunterCount >= 2 && gameTime > 0)
        {
            HidersWins();
        }


        //EVENTOS EXTRA
        if(gameTime == 150)
        {
            //DoorHunters
        }
        if(hidersCount > 2 && hunterCount > 1)
        {
            //Salida Secreta
            //Condicion si escapan
        }
    }

    private void HidersWins()
    {
        //EndGame y deshabilitar todo, menos WinCanvas +Añadir camara en el canvas?
        EndGameCanvas.SetActive(true);
        hidersWinScreen.SetActive(true);
    }
}
