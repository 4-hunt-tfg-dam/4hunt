﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HiderHP : MonoBehaviour
{
    private PhotonView PV;
    public int health = 100;
    public int countAux;
    public HealthBar healthBar;
   // public GameObject textureBlood;
    private Camera deathCamera;


    private void Awake()
    {
        //deathCamera = Camera.FindObjectOfType<Camera>();
        PV = GetComponent<PhotonView>();
    }
    //public GameObject effectDeath; //todavia no tenemos este efecto creado.
    public void HealthDown(int damage) //public para poder llamar al metodo desde PlayerAimGun
    {
        
        health = health - damage;
        Debug.Log("vida"+ health);
        healthBar.SetHealth(health);
       // Instantiate(textureBlood, transform.position, Quaternion.identity);

        if (health <= 0)
        {
            PV.RPC("PlayerDead", RpcTarget.All);
        }
    }

    [PunRPC]
    void PlayerDead()
    {
        //isAlive = RoomManager.Instance.isAlive;
        //RoomManager.Instance.SetTeam();
        //  PV.RPC("PlayerDead", RpcTarget.OthersBuffered, myTeam);
        countAux = RoomManager.Instance.HiderDeaths++;
        Die();

    }
    void Die()
    {
        //Death Effect + change camera
        Destroy(gameObject);
    }
}
