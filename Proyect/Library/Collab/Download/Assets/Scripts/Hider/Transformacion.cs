﻿using Cainos.PixelArtTopDown_Basic;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transformacion : MonoBehaviour
{

    [SerializeField] private GameObject hiderPlayer;
    // [SerializeField] private SpriteRenderer hiderSprite;

    [HideInInspector] public GameObject rc;
    private SpriteRenderer originalSprite;

    private PhotonView PV;
    private Vector2 boxSize = new Vector2(0.1f, 1f);


    // Start is called before the first frame update
    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        originalSprite = hiderPlayer.transform.GetComponent<SpriteRenderer>();
    }



    // Update is called once per frame
    private void Update() {

        if (Input.GetMouseButtonDown(0))
        {
            PV.RPC("Transform", RpcTarget.All);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            PV.RPC("ReverseTransform", RpcTarget.All);
        }
         /*  RaycastHit2D collideInfo = Physics2D.Raycast(hiderRayCastPos.transform.position, GetMousePosition());
            Debug.Log(collideInfo.transform.name);
      


            if (prop != null && collideInfo.transform.tag == "prop")
            {
                Debug.Log("Aqui deberia llegar" + collideInfo.transform.tag);
                // hiderPlayer.transform.position = hiderPlayer.GetComponentInChildren<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
                                                                                                                                                                         
                //  hiderPlayer.transform.position = cameraCenter.position;
                //  hiderPlayer.transform.localScale = prop.transform.localScale;
 
                hiderSprite.sprite = prop.sprite;
                hiderPlayer.GetComponent<BoxCollider2D>().size = propCollide.size;
                hiderPlayer.GetComponent<BoxCollider2D>().offset = propCollide.offset;
                // hiderSprite.GetComponent<SpriteRenderer>().size = prop.size;
               // hiderPlayer.transform.position = new Vector3(hiderPlayer.transform.position.x, hiderPlayer.transform.position.y-2, hiderPlayer.transform.position.z); // solo cambiar la altura, el resto de valroes de Vector3 se quedan fijos.
            }
}
    */
     }


    [PunRPC]
    public void CheckTransform()
    {
        RaycastHit2D[] hits = Physics2D.BoxCastAll(transform.position, boxSize, 0, Vector2.zero);

        if(hits.Length > 0)
        {
            foreach(RaycastHit2D rc in hits)
            {
                if (rc.transform.GetComponent<PropsAltar>().interactable)
                {
                    hiderPlayer.GetComponent<TopDownCharacterController>().speed = 3;
                    hiderPlayer.GetComponent<Animator>().enabled = false; 
                    SpriteRenderer prop = rc.transform.GetComponent<SpriteRenderer>();
                    BoxCollider2D propCollide = rc.transform.GetComponent<BoxCollider2D>();
                    hiderPlayer.GetComponent<SpriteRenderer>().sprite = prop.sprite;
                    hiderPlayer.GetComponent<SpriteRenderer>().size = prop.size;
                    hiderPlayer.GetComponent<BoxCollider2D>().size = propCollide.size;
                    hiderPlayer.GetComponent<BoxCollider2D>().offset = propCollide.offset;
                    return;
                }
            }
        }
    }

    [PunRPC]
    public void Transform()
    {
      // rc.transform.GetChild(1).gameObject.SetActive(true);//Activar Highligth
        if (rc.GetComponent<PropsAltar>().interactable)
                {
                    hiderPlayer.GetComponent<TopDownCharacterController>().speed = 3;
                    hiderPlayer.GetComponent<Animator>().enabled = false;
                    SpriteRenderer prop = rc.transform.GetComponent<SpriteRenderer>();
                    BoxCollider2D propCollide = rc.transform.GetComponent<BoxCollider2D>();
                    hiderPlayer.GetComponent<SpriteRenderer>().sprite = prop.sprite;
                    hiderPlayer.GetComponent<SpriteRenderer>().size = prop.size;
                    hiderPlayer.GetComponent<BoxCollider2D>().size = propCollide.size;
                    hiderPlayer.GetComponent<BoxCollider2D>().offset = propCollide.offset;
                }
            }
        
    
    [PunRPC]
    public void ReverseTransform()
    {
        hiderPlayer.GetComponent<TopDownCharacterController>().speed = 5;
        hiderPlayer.GetComponent<Animator>().enabled = true;
        hiderPlayer.GetComponent<SpriteRenderer>().sprite = originalSprite.sprite;
        hiderPlayer.GetComponent<SpriteRenderer>().size = originalSprite.size;
        hiderPlayer.GetComponent<BoxCollider2D>().size = new Vector2(0.3392634f, 0.4894094f); 
        hiderPlayer.GetComponent<BoxCollider2D>().offset = new Vector2(-0.0001475811f, 0.003345728f); ;


    }
    public static Vector3 GetMousePosition()
    {
        Vector3 vec = GetMousePositionZ(Input.mousePosition, Camera.main);
        vec.z = 0f;
        return vec;
    }

    public static Vector3 GetMousePositionZ()
    {
        return GetMousePositionZ(Input.mousePosition, Camera.main);
    }

    public static Vector3 GetMousePositionZ(Camera worldCamera)
    {
        return GetMousePositionZ(Input.mousePosition, worldCamera);
    }

    public static Vector3 GetMousePositionZ(Vector3 screenPosition, Camera worldCamera)
    {
        Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }

  
}
