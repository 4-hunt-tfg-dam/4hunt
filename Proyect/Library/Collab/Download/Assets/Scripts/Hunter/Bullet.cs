﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
   public GameObject effectColisision;
   public HunterHP hunter;
    private PhotonView PV;


    public void Awake()
    {
        PV = GetComponent<PhotonView>();
        //hunter.GetComponent<BoxCollider2D>().isTrigger = true; 
        //PV.ViewID = 1019;
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        HiderHP hider = collision.transform.GetComponent<HiderHP>();
       // hunter = HunterHP.FindObjectOfType<HunterHP>();
        GameObject auxEffect  = Instantiate(effectColisision, transform.position, Quaternion.identity);
        hunter = GetComponentInParent<HunterHP>();


        if (collision.transform.tag == "Hider")
         {
            Debug.Log("Daño hider");
            hider.HealthDown(10);
            Destroy(gameObject);
            auxEffect.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0, 1); //diferente color al dar a un hider
        }
        else if(collision.transform.tag == "prop")//si golpea a cualquier cosa que no sea un hider pierde vida ((Fixear en el futuro con props/walls
         {
            hunter.HealthDown(10);
        }else
        {
            Debug.Log("Daño a nadie  " + collision.transform.tag);
        }
         Destroy(auxEffect, 0.3f);
         Destroy(gameObject);
        hunter.GetComponentInParent<BoxCollider2D>().isTrigger = true;
    }

    private void Update()
    {
        Destroy(gameObject, 1f);
    }
}
 
    