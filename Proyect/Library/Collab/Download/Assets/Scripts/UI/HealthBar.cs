﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
  
    public Slider sliderHealth;
    public Gradient changeColor;
    public Image HP;
    public Slider sliderAmmo;
    public PhotonView PV;

    private void Awake()
    {
        if (!PV.IsMine)
            Destroy(gameObject);
    }

    public void SetMax(int health)
    {
        sliderHealth.maxValue = health;
        sliderHealth.value = health;

       HP.color = changeColor.Evaluate(1f);
    }
  public void SetHealth(int health)
    {
        sliderHealth.value = health;
        HP.color = changeColor.Evaluate(sliderHealth.normalizedValue);
    }

    public void setAmmo(int ammo)
    {
        if (ammo == 3)
        {
            sliderAmmo.value = 100;
        } else if (ammo == 2)
        {
            sliderAmmo.value = 96;
        }else if (ammo == 1)
        {
            sliderAmmo.value = 92;
        }
        else
            sliderAmmo.value = 89;
    }
}
