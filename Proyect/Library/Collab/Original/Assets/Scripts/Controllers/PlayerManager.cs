﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO; //For prefabs paths.
public class PlayerManager : MonoBehaviour
{

    public PhotonView PV;
    public int myTeam;
    public static PlayerManager Instance;
    public GameObject myAvatar;
    private int sceneAct;
    private int deathCounter;
    private Camera deathCameraHunter;
    private Camera deathCameraHider;

    // Update is called once per frame
    void Start()
    {
        PV = GetComponent<PhotonView>();
        if (PV.IsMine)//Photonview unique owner.
        {
            PV.RPC("RPC_GetTeam", RpcTarget.MasterClient);
           
        }
        Instance = this;
    }

    void Update()
    {
        sceneAct = RoomManager.Instance.sceneActual;
        if (sceneAct == 1)
        {
            if (myAvatar == null && myTeam != 0)
            {
                if (myTeam == 1)
                {
                    deathCounter = RoomManager.Instance.HunterDeaths;
                    if (PV.IsMine && deathCounter == 0)
                    {
                        Transform SpawnHunters = RoomManager.Instance.GetSpawnPoint(myTeam);
                        myAvatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/MapAliens", "HunterController"), SpawnHunters.position, SpawnHunters.rotation, 0);
                    }else //Si ya hay muertos no respawnear, coger la camara de otro de tu equipo
                    {
                        deathCameraHunter = GameObject.Find("HunterDeath").GetComponent<Camera>();
                        deathCameraHunter.gameObject.SetActive(true);
                    }
                }
                else if (myTeam == 2)
                {
                    deathCounter = RoomManager.Instance.HiderDeaths;
                    if (PV.IsMine && deathCounter == 0)
                    {
                        Transform SpawnHiders = RoomManager.Instance.GetSpawnPoint(myTeam);
                        //Se hace con path porque PhotonNetwork solo recibe strings, no prefabs como tal.
                        myAvatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/MapAliens", "HiderController"), SpawnHiders.position, SpawnHiders.rotation, 0);
                    }else
                    {
                        deathCameraHider = GameObject.Find("HiderDeath").GetComponent<Camera>();
                        deathCameraHider.gameObject.SetActive(true);
                    }
                }
            }
        }
        if (sceneAct == 2)
        {
            if (myAvatar == null && myTeam != 0)
            {
                if (myTeam == 1)
                {
                    deathCounter = RoomManager.Instance.HunterDeaths;
                    if (PV.IsMine && deathCounter == 0)
                    {
                        Transform SpawnHunters = RoomManager.Instance.GetSpawnPoint(myTeam);
                        myAvatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/AntiqueMap", "HunterController"), SpawnHunters.position, SpawnHunters.rotation, 0);
                    }else
                    {
                        deathCameraHunter = GameObject.Find("HunterController").GetComponent<Camera>();
                        deathCameraHunter.gameObject.SetActive(true);
                    }
                }
                else if (myTeam == 2)
                {
                    deathCounter = RoomManager.Instance.HiderDeaths;
                    if (PV.IsMine && deathCounter == 0)
                    {
                        Transform SpawnHiders = RoomManager.Instance.GetSpawnPoint(myTeam);
                        //Se hace con path porque PhotonNetwork solo recibe strings, no prefabs como tal.
                        myAvatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs/AntiqueMap", "HiderController"), SpawnHiders.position, SpawnHiders.rotation, 0);
                    }else
                    {
                        deathCameraHider = GameObject.Find("HiderController").GetComponent<Camera>();
                        deathCameraHider.gameObject.SetActive(true);
                    }
                }
            }
        }
    


    }
    [PunRPC]//BUSCAR DOC
    void RPC_GetTeam()
    {
        myTeam = RoomManager.Instance.nextPlayersTeam;
        RoomManager.Instance.SetTeam(); 
        PV.RPC("RPC_SendTeam",RpcTarget.OthersBuffered, myTeam);
    }

    [PunRPC]
    void RPC_SendTeam(int team)
    {
        //Envia la info del team al resto de jugadores
        myTeam = team;
    }

  
}
